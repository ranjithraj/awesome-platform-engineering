# Awesome Platform Engineering [![Awesome](https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg)](https://github.com/sindresorhus/awesome)

A curated list of awesome tools, platforms, and resources for platform engineering.


[[_TOC_]]

# About 

## Meta | Awesome lists:

- **[Awesome Sysadmin](https://github.com/awesome-foss/awesome-sysadmin)**: A curated list of open source sysadmin resources including tools for monitoring, configuration management, backup, and more.

- **[Awesome DevOps](https://github.com/tnightengale/awesome-devops)**: A comprehensive collection of resources related to DevOps, including CI/CD tools, infrastructure as code, automation frameworks, and best practices.

- **[Awesome Infrastructure as Code](https://github.com/dastergon/awesome-infrastructure-as-code)**: A collection of resources and tools for Infrastructure as Code (IaC), including Terraform, Ansible, and CloudFormation.

- **[Awesome Cloud Native](https://github.com/rootsongjc/awesome-cloud-native)**: A curated list of cloud-native tools and resources, including service meshes, observability, and serverless frameworks.

- **[Awesome SRE](https://github.com/dastergon/awesome-sre)**: A curated list of Site Reliability Engineering (SRE) resources, including best practices and tools.

- **[Awesome Monitoring](https://github.com/crazy-canux/awesome-monitoring)**: A list of monitoring tools and resources, covering various aspects of system, application, and network monitoring.

- **[Awesome CI/CD](https://github.com/ciandcd/awesome-ciandcd)**: A comprehensive collection of CI/CD tools, practices, and resources to streamline the software development lifecycle.
- **[Awesome Scalability](https://github.com/binhnguyennus/awesome-scalability)**: A curated list of high scalability, high availability, and high stability resources.

- **[Awesome Immutable Infrastructure](https://github.com/mbiesiad/awesome-immutable-infrastructure)**: A curated list of resources on immutable infrastructure.
- **[Awesome Kubernetes](https://github.com/ramitsurana/awesome-kubernetes)**: A collection of Kubernetes resources including tutorials, tools, and best practices for managing containerized applications.

- **[Awesome Docker](https://github.com/veggiemonk/awesome-docker)**: A curated list of Docker resources, such as tutorials, tools, and projects, which are essential for containerization and microservices architecture.

- **[Awesome Prometheus](https://github.com/roaldnefs/awesome-prometheus)**: A list of Prometheus resources for monitoring and alerting, including exporters, integrations, and tools to enhance its functionality.

- **[Awesome Terraform](https://github.com/shuaibiyy/awesome-terraform)**: A collection of Terraform resources, such as modules, tools, and tutorials, which are crucial for infrastructure as code practices.

- **[Awesome Jenkins](https://github.com/ciscocsirt/awesome-jenkins)**: A list of Jenkins resources, plugins, and integrations to help with setting up and optimizing CI/CD pipelines.

- **[Awesome Ansible](https://github.com/ansible-community/awesome-ansible)**: A curated list of Ansible resources, including playbooks, roles, and collections, for configuration management and automation.

# Contributing

Contributions are welcome! Please read the [CONTRIBUTING.md](CONTRIBUTING.md) for guidelines.

