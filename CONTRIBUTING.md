# Contributing to Awesome Platform Engineering

Thanks for your interest in contributing to this list! We welcome additions, corrections, and improvements from the community.

## How to Contribute

1. **Fork the repository:** Click the "Fork" button at the top right of this repository's page.
2. **Create a branch:** `git checkout -b your-branch-name` (replace `your-branch-name` with a descriptive name)
3. **Add your contribution:** 
   - Choose the appropriate category for your item.
   - Add your entry in alphabetical order.
   - Follow the existing formatting. Here's the standard format:
     ```
     * **[Tool Name]:** [Brief description] ([Link])
     ```
   - If you're adding a new category, make sure it's a relevant and significant area in platform engineering.
4. **Commit your changes:** `git commit -m "Add [Tool Name] to [Category]"`
5. **Push to your branch:** `git push origin your-branch-name`
6. **Open a pull request (PR):** Go to the original repository and click the "New pull request" button.  Fill out the PR template with a clear description of your changes.

## Guidelines

* **Relevance:** Ensure the tool/resource is directly relevant to platform engineering.
* **Quality:** Prioritize mature, well-maintained, and actively developed projects.
* **Uniqueness:** Avoid duplicates. If a tool is already listed, consider adding a more detailed description or a new category if it fits.
* **Formatting:** Follow the existing formatting conventions to maintain consistency.
* **No Self-Promotion:** Avoid excessive promotion of your own projects.
* **Be Respectful:** Maintain a positive and constructive tone in your contributions and interactions.

## Reviewing Pull Requests

We'll review your pull request as soon as possible. Please be patient, as it may take some time for us to thoroughly assess your contribution. We might suggest changes or ask for more information.

## Code of Conduct

Please note that this project is released with a [Contributor Code of Conduct](CODE_OF_CONDUCT.md). By participating in this project you agree to abide by its terms.

## Thank You!

Your contributions help make this list a valuable resource for the platform engineering community. We appreciate your time and effort!
